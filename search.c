#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20
#define MAX_RAND 100

void initial_arr(int a[]);
void line_search_left(int a[], int num);
void line_search_right(int a[], int num);
void output(int a[]);
void find_max(int a[]);
void find_min(int a[]);
void find_two_max(int a[]);
void find_even_min(int a[]);

void initial_sort_array(int a[], int len);
void binary_search(int a[], int key, size_t low, size_t high);
void print_divider(size_t len);

int main(void){
    int arr[SIZE] = {0};
    int arr_sort[SIZE] = {0};
    int number;

    srand(time(NULL));

    initial_arr(arr);

    printf("%s", "Enter number to find: ");
    scanf("%d", &number);

    //LINE SEARCH - O(N) complexity
    printf("%s", "\n-------------- LINEAR SEARCH --------------\n\n");
    line_search_left(arr, number);
    line_search_right(arr, number);
    find_max(arr);
    find_min(arr);
    find_two_max(arr);
    find_even_min(arr);
    output(arr);

    //BINARY SEARCH - O(log2N) complexity
    printf("%s", "\n-------------- BINARY SEARCH --------------\n\n");
    initial_sort_array(arr_sort, SIZE);
    output(arr_sort);
    binary_search(arr_sort, number, 0, SIZE - 1);


    return 0;
}

//initialize array by random numbers from 1 to MAX_RAND 
void initial_arr(int a[]) {
    size_t i;
    for(i = 0; i < SIZE; i++) {
        a[i] = rand() % MAX_RAND + 1;
    }
}

//find first index of num in a[], if not num, print -1
void line_search_left(int a[], int num) {
    size_t i;
    int resault = -1;
    for(i = 0; i < SIZE; i++) {
        if (a[i] == num && resault == -1) {
            resault = i;
        }
    }
    printf("Index of first meeting: %d\n", resault);
}

//find last index of num in a[], if not num, print -1
void line_search_right(int a[], int num) {
    size_t i;
    int resault = -1;
    for(i = 0; i < SIZE; i++) {
        if (a[i] == num) {
            resault = i;
        }
    }
    printf("Index of last meeting: %d\n", resault);
}

//find max number in array
void find_max(int a[]) {
    int maximum = a[0];
    size_t i;
    for(i = 0; i < SIZE; i++){
        if (a[i] > maximum) {
            maximum = a[i];
        }
    }
    printf("Maximum value is: %d\n", maximum);
}

//find min number in array
void find_min(int a[]) {
    int minimum = a[0];
    size_t i;
    for(i = 0; i < SIZE; i++) {
        if (a[i] < minimum) {
            minimum = a[i];
        }
    }
    printf("Minimum value is: %d\n", minimum);
}

//find first max number and second max number
void find_two_max(int a[]) {
    int maximum1, maximum2;
    size_t i, index1, index2;

    if (a[0] >= a[1]) {
        maximum1 = a[0];
        index1 = 0;
        maximum2 = a[1];
        index2 = 1;
    } else {
        maximum1 = a[1];
        index1 = 1;
        maximum2 = a[0];
        index2 = 0;
    }

    for (i = 2; i < SIZE; i++) {
        if (a[i] > maximum1) {
            maximum2 = maximum1;
            index2 = index1;
            maximum1 = a[i];
            index1 = i;
        } else if (a[i] > maximum2) {
            maximum2 = a[i];
            index2 = i;
        }
    }

    printf("First maximum value is %d with index %lu\n" 
    "Second maximum value is %d with index %lu\n", maximum1, index1, maximum2, index2);
}

//find minimum even number
void find_even_min(int a[]) {
    int even = -1;
    size_t i;
    for (i = 0; i < SIZE; i++) {
        if ((a[i] % 2 == 0) && (even == -1 || a[i] < even)) {
            even = a[i];
        }
    }
    printf("Minimum even number: %d\n", even);
}

//output array
void output(int a[]) {
    size_t i;
    printf("%s", "Array: ");
    for(i = 0; i < SIZE; i++) {
        i == SIZE - 1 ? printf("%d\n", a[i]) : printf("%d ", a[i]);
    }
}

//initialize_sort_array
void initial_sort_array(int a[], int len) {
    int i;
    for(i = 0; i < len; i++) {
        a[i] = i * 2 + i + 1;
    }
}

//binary search
void binary_search(int a[], int key, size_t low, size_t high) {
    size_t i;
    size_t mid;
    int index = -1;
    print_divider(SIZE);
    while(low <= high && index != (int)mid) {
        mid = (low + high) / 2;
        if (a[mid] == key){
            index = mid;
        } else if (key < a[mid]) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
        for (i = 0; i < SIZE; i++) {
            if (index != (int)mid) {
                (i <= high && i >= low) ? printf("%2d ", a[i]) : printf("%2s ", " ");
            } else {
                (i == mid) ? printf("%2d ", a[i]) : printf("%2s ", " ");
            }
        }
        puts("");
    }
    print_divider(SIZE);
    printf("Index of search element is %d\n", index);
}

void print_divider(size_t len) {
    size_t i;
    for(i = 0; i < len * 3; i++){
        printf("%c", '_');
    }
    puts("");
}