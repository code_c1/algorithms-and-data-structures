#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20
#define BASE 100

void fill_array(int a[]);
void output(int a[]);
void bubble_sort(int * const a, size_t len, int (*compare)(int a, int b));
void swap(int *ptr1elem, int *ptr2elem);
int ascending(int a, int b);
int descending(int a, int b);


int main(void) {
    int arr[SIZE];
    int check;

    srand(time(NULL));

    fill_array(arr);
    printf("%s", "Start sequence:\n");
    output(arr);
    puts("Enter '1' to ascending sort or '2' to descending sort:");
    scanf("%d", &check);
    if (check == 1) {
        bubble_sort(arr, SIZE, ascending);
    } else if (check == 2) {
        bubble_sort(arr, SIZE, descending);
    }
    printf("%s", "Sort sequence:\n");
    output(arr);

    return 0;
}

void fill_array(int a[]) {
    size_t i;
    for(i = 0; i < SIZE; i++){
        a[i] = rand() % BASE + 1;
    }
}

void output(int a[]){
    size_t i;
    for(i = 0; i < SIZE; i++){
        i == SIZE - 1 ? printf("%d\n", a[i]) : printf("%d ", a[i]);
    }
}

void bubble_sort(int * const a, size_t len, int (*compare)(int a, int b)){
    size_t i, j;
    for (i = 1; i < len; i++){
        for(j = 0; j < len - 1; j++){
            if((*compare)(a[j], a[j + 1])){
                swap(&a[j], &a[j + 1]);
            }
        }
    }
}

void swap(int *ptr1elem, int *ptr2elem) {
    int temp = *ptr1elem;
    *ptr1elem = *ptr2elem;
    *ptr2elem = temp;
}

int ascending(int a, int b) {
    return a > b;
}

int descending(int a, int b) {
    return a < b;
}