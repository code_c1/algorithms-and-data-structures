#include <stdio.h>
#include <string.h>

void rle(char s[]);

int main(void) {
    char str[] = "AAAABBBBCCDDDDEFGGGGGGHIIJKKKKKKKKKK";
    rle(str);
    return 0;
}

void rle(char s[]) {
    size_t len = strlen(s);
    size_t i;
    size_t last_index = 0;
    size_t quantity = 0;
    char temp = s[0];
    char new_str[len];
    for(i = 1; i <= len; i++) {
        if (s[i] != temp) {
            quantity = i - last_index;
            char temp_str[len];
            sprintf(temp_str, "%c%lu", temp, quantity);
            strcat(new_str, temp_str);
            temp = s[i];
            last_index = i;
        }
    }
    printf("New string: %s", new_str);
}